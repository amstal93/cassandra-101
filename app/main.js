var express = require('express')
var bodyParser = require('body-parser')
var cassandra = require('cassandra-driver')

var client = new cassandra.Client({
  contactPoints: ['cassandra_1', 'cassandra_2'],
  keyspace: 'pp_ex_keyspace'
})

var app = express();
app.use(bodyParser.json())

app.get('/all', function(req, res) {
  const query = 'SELECT * FROM users'

  console.log("Getting all users")

  client.execute(query, [], function(err, cassandra_result) {
    if (err) {
      res.status(500).json(err)
    } else {
      res.send(cassandra_result)
    }
  })
})

app.get('/group/:group', function(req, res) {
  const query = 'SELECT * FROM users WHERE group = ?'

  var group = parseInt(req.params.group)

  console.log('Getting users for group: %d', group)

  client.execute(query, [group], { prepare: true }, function(err, cassandra_result) {
    if (err) {
      res.status(500).json(err)
    } else {
      res.send(cassandra_result)
    }
  })
})

app.post('/new', function(req, res) {
  const query = 'INSERT INTO users(user_id, name, group) VALUES (uuid(), ?, ?)'

  var name = req.body.name
  var group = parseInt(req.body.group)

  console.log('Adding new user, name: %s, group: %d', name, group)

  client.execute(query, [name, group], { prepare: true }, function(err, cassandra_result) {
    if (err) {
      res.status(500).json(err)
    } else {
      res.send(cassandra_result)
    }
  })
})

app.listen(3000, function() {
  console.log('App listening on port 3000')
})
